const CopyWebpackPlugin = require('copy-webpack-plugin'),
 HtmlWebpackPlugin = require('html-webpack-plugin'),
 {VueLoaderPlugin} = require('vue-loader'),

 utils = require('./utils');

module.exports = {
    entry: {
        main: './src/index.js',
    },
    module: {
        rules: [
            {
                test: /\.vue$/u,
                use: 'vue-loader',
            },
            {
                enforce: 'pre',
                exclude: /node_modules/u,
                loader: 'eslint-loader',
                options: {
                    cache: true,
                    emitWarning: true,
                },
                test: /\.js$/u,
            },
            {
                // eslint-disable-next-line wrap-regex
                exclude: file => /node_modules/u.test(file) &&
                                    // eslint-disable-next-line wrap-regex
                                    !/\.vue\.js/u.test(file),
                test: /\.js$/u,
                use: ['babel-loader'],
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/u,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        // eslint-disable-next-line max-len
                        name: utils.assetsPath('img/[name].[contenthash:7].[ext]'),
                    },
                },
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/u,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        // eslint-disable-next-line max-len
                        name: utils.assetsPath('media/[name].[contenthash:7].[ext]'),
                    },
                },
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/u,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 10000,
                        // eslint-disable-next-line max-len
                        name: utils.assetsPath('fonts/[name].[contenthash:7].[ext]'),
                    },
                },
            },
        ],
    },
    output: {
        filename: 'assets/js/bundle.[hash].js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            cache: true,
            favicon: 'public/favicon.ico',
            filename: 'index.html',
            inject: true,
            template: './public/index.html',

        }),
        new VueLoaderPlugin(),
        new CopyWebpackPlugin([
        {
            from: utils.resolve('static/img'),
            to: utils.resolve('dist/static/img'),
            toType: 'dir',
        },
        {
            from: utils.resolve('static/media'),
            to: utils.resolve('dist/static/media'),
            toType: 'dir',
        },
        ]),
    ],
    resolve: {
        alias: {
            'assets': utils.resolve('assets'),
            'components': utils.resolve('src/components'),
            'pages': utils.resolve('src/pages'),
            'static': utils.resolve('static'),
        },
        extensions: [
                    '.js',
                    '.vue',
                    '.json'
                    ]
    },
}