const MiniCssExtractPlugin = require('mini-css-extract-plugin'),
      OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
      TerserPlugin = require('terser-webpack-plugin'),
      baseConfig = require('./webpack.config.base'),
      merge = require('webpack-merge'),
      webpack = require('webpack');


module.exports = merge(baseConfig, {
  devtool: 'source-map',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.css?$/u,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
            },
          },
        ],
      },
      {
        test: /\.scss$/u,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/u,
        use: {
            loader: 'file-loader?name=img/[name].[contenthash:7].[ext]',
            options: {
                emitFile: false
            },
        },
    },
    ],
  },
  optimization: {
    minimizer: [
      new OptimizeCSSAssetsPlugin({}),
      new TerserPlugin({
        cache: true,
        extractComments: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
          output: {
            // eslint-disable-next-line require-unicode-regexp
            comments: /@license/i
          }
        },
      }),
    ],
    nodeEnv: 'production',
    splitChunks: {
      cacheGroups: {
        commons: {
          chunks: 'all',
          filename: 'assets/js/vendor.[contenthash].js',
          test: /[\\/]node_modules[\\/]/u,
        },
      },
    },
  },
  plugins:
    [
      new MiniCssExtractPlugin({
        chunkFilename: 'assets/css/[id].[contenthash].css',
        filename: 'assets/css/[name].[contenthash].css',
      }),
      new webpack.EnvironmentPlugin({
        NODE_ENV: 'production',
      }),
    ],
});