FROM node:11

# RUN apk add --update bash && rm -rf /var/cache/apk/*
WORKDIR /var/www/

ENV NODE_ENV=development
COPY package.json /var/www/package.json
RUN yarn install

COPY . /var/www

EXPOSE 8080
VOLUME /var/www

CMD ["yarn", "run", "dev:docker"]