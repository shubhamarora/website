import '../assets/main.scss';

import App from './App.vue';
import Vue from 'vue';
import Vuex from 'vuex';
import {library} from '@fortawesome/fontawesome-svg-core';
import {faTwitter, faGithub, faGitlab}
from '@fortawesome/free-brands-svg-icons';
import {faEnvelope} from '@fortawesome/free-regular-svg-icons';
import {faHeart} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';


// Add icon here
library.add(faTwitter, faGithub, faGitlab, faEnvelope, faHeart);

const VueScrollTo = require('vue-scrollto');

Vue.use(VueScrollTo);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.use(Vuex);

Vue.config.productionTip = false;

Vue.directive('scroll', {

  inserted (el, binding) {

    const f = event => {

      if (binding.value(event, el)) {

        window.removeEventListener('scroll', f)

      }

    }

    window.addEventListener('scroll', f)

  }
});

Vue.use(VueScrollTo, {
  cancelable: true,
  container: 'body',
  duration: 1000,
  easing: 'ease-out',
  force: true,
  offset: 0,
  onCancel: false,
  onDone: false,
  onStart: false,
  x: false,
  y: true
});

new Vue({
  render: h => h(App),
}).$mount('#app');